def list_comp():
    x = int(input('Type an integer: '))
    y = int(input('Type another integer: '))
    z = int(input('Type another integer: '))
    n = int(input('One more integer: '))
    # my_list = []
    # for a in range(x + 1):
    #     for b in range(y + 1):
    #         for c in range(z + 1):
    #             if a + b + c != n:
    #                 my_list.append([a, b, c])
    # print(my_list)
    print([[a, b, c] for a in range(x + 1) for b in range(y + 1) for c in range(z + 1) if a + b + c != n])


def runner_up():
    n = int(input('Type the number of integers (between 2 and 10): '))
    arr = map(int, input('Type %s integers separated by space: ' % n).split())
    result = sorted(set(arr))[-2]
    print('The second maximum is %s' % result)


def nested_lists():
    n = int(input('Type the number of students: '))
    students = [[input('Name: '), float(input('Grade: '))] for _ in range(n)]
    print(students)
    # result = []
    # for name, marks in students:
    #     result.append(marks)
    # print(sorted(set(result)))[1]
    result = sorted(set(marks for name, marks in students))[1]
    final = [name for name, marks in sorted(students) if marks == result]
    for student in final:
        print(student)


def finding_the_percentage():
    n = int(input('Type the number of students: '))
    student_marks = {}
    for _ in range(n):
        name, *line = input('Type the name and scores separated by space: ').split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input('Choose a name: ')
    marks_list = student_marks[query_name]
    if query_name in student_marks:
        average_marks = sum(marks_list) / len(marks_list)
        print(format(average_marks, '.2f'))

# Neterminata
def my_lists():
    my_list = []
    n = int(input('Type the number of commands: '))
    for _ in range(n):
        inserted = input('Type the command and/or element separated by space: ').split()
        command = inserted[0]
        element = inserted[1:]
        print(command)
        print(element)
    print(my_list)


def my_tuples():
    n = int(input('Type the number of elements: '))
    integer_list = map(int, input('Type de elements separated by space: ').split())
    l = [x for x in integer_list]
    print(hash(tuple(l)))

if __name__ == '__main__':
    list_comp()
    runner_up()
    nested_lists()
    finding_the_percentage()
    # my_lists()
    my_tuples()