def swap_case(s):
    # new_str = ''
    # for letter in s:
    #     if letter.isupper():
    #         new_str += letter.lower()
    #     elif letter.islower():
    #         new_str += letter.upper()
    #     else:
    #         new_str += letter
    # print(new_str)
    print(''.join(l.lower() if l.isupper() else l.upper() for l in s))


def split_and_join(line):
    # line = line.split()
    # print('-'.join(line))
    print(line.replace(' ', '-'))


def print_full_name(a, b):
    print("Hello %s %s! You just delved into python." % (a, b))


def mutate_string(string, position, character):
    print(string[:position] + character + string[position+1:])


if __name__ == '__main__':
    swap_case('HackerRank.com presents "Pythonist 2".')
    split_and_join('this is a string')
    print_full_name('Ross', 'Taylor')
    mutate_string('abracadabra', 5, 'k')
