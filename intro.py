def hello():
    print("Hello, World!")


def if_else():
    n = int(input('Type a positive number: '))
    if n % 2 == 1:
        print('Weird')
    elif n % 2 == 0 and n in range(2, 6):
        print('Not Weird')
    elif n % 2 == 0 and n in range(6, 21):
        print('Weird')
    elif n % 2 == 0 and n > 20:
        print('Not Weird')
    else:
        print('You are joking!')


def arithmetic():
    a = int(input('Type a number: '))
    b = int(input('Type another number: '))
    print('%s + %s = %s' % (a, b, a + b))
    print('%s - %s = %s' % (a, b, a - b))
    print('%s * %s = %s' % (a, b, a * b))


def division():
    a = int(input('Type a number: '))
    b = int(input('Type another number: '))
    print('Integer division (%s by %s): %s' % (a, b, a // b))
    print('Float division (%s by %s): %s' % (a, b, a / b))


def loops():
    n = int(input('Type a positive number: '))
    for i in range(n):
        print('%s * %s is %s' % (i, i, i**2))


def is_leap(year):
    if year % 400 == 0:
        print('%s is a leap year' % year)
        return True
    elif year % 100 == 0:
        print('%s is not a leap year' % year)
        return False
    elif year % 4 == 0:
        print('%s is a leap year' % year)
        return True
    print('%s is not a leap year' % year)
    return False


def print_func():
    n = int(input('Type a positive number: '))
    for i in range(1, n + 1):
        print(i, end='')  # echivalentul lui (print i,) in python 2


if __name__ == '__main__':
    hello()
    if_else()
    arithmetic()
    division()
    loops()
    is_leap(1990)
    print_func()
